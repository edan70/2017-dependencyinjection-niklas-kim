# Dependency Injection in Java #

In this ExtendJ module we have added the possibility for a programmer to inject interface initializations with a class implementing that interface. The way it works is that the compiler will parse a interface initialization and decide whether it is able to perform an injection or not by mapping the interface type to a whitelist of supported interfaces. If the interface is supported it will run an algorithm that replaces the interface initialization with the deemed most fitting class that implements that interface. First a static semantic analysis is done and if it is deemed neccessary an actual dynamic evaluation of the execution time between the different implementations of the interface is done.

## Dependencies ##

* Git
* Gradle
* ExtendJ
* Ant
* JDK

## Building ##

*** The cloning is not working atm as the repo is private.
Cristoph is the owner, will let him know during next meeting. ***

Start of by cloning the repository by typing
```
git clone --recursive git@bitbucket.org:edan70/2017-dependencyinjection-niklas-kim.git
```
The --recursive is important as this fetches the submodule extendj which our extension is dependant on.

The compiler can be built by running `make` when inside of the extension directory.
This will clean up and rebuild the `.jar` file.

A `.jar` file named `extension.jar` has then been created and can be used to compile java code into java class files.
E.g:
```
java -jar extension.jar TestJavaFile.java
```
This will output a class file `TestJavaFile.class` in the same directory which then can be run by the `java TestJavaFile` command in a terminal window.

## Testing ##
Testing of the extension can be done by running
```make test```
These will run test cases and all should pass.
The program will run 150 times for every possible implementation of a interface to measure a mean time in the dynamic analysis.

### Acknowledgements ###

Christoph Reichenbach - Supervisor - For guiding us in the right direction.

### Who do I talk to? ###

* Niklas Jönsson - dat13njo@student.lu.se
* Kim Fransson - dat13kfr@student.lu.se
