import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

public class TestAutoboxing {

  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    Set<String> a_a = new HashSet<>();
    Set<String> a_b = new HashSet<String>();
    Set<String> a_c = new Set<>();
    Set<String> a_d = new Set<String>();

    List<String> b_a = new LinkedList<>();
    List<String> b_b = new LinkedList<String>();
    List<String> b_c = new List<>();
    List<String> b_d = new List<String>();

    Map<String, String> c_a = new HashMap<>();
    Map<String, String> c_b = new HashMap<String, String>();
    Map<String, String> c_c = new Map<>();
    Map<String, String> c_d = new Map<String, String>();

  }
}
