import sys
import numpy as np
from scipy import stats
def read_file(filename):
    lines = []
    with open(filename) as filestream:
        for line in filestream:
            lines.append(float(line.rstrip()))
    return lines


def calc_conf(file):
    times = read_file(file + ".msr")
    n = len(times) / 2
    orig_times = times[:n]
    orig_mean, orig_sigma = np.mean(orig_times), np.std(orig_times)
    conf_int_orig = stats.norm.interval(0.90, loc=orig_mean, scale=orig_sigma)

    opti_times = times[n:]
    opti_mean, opti_sigma = np.mean(opti_times), np.std(opti_times)
    conf_int_opti = stats.norm.interval(0.90, loc=opti_mean, scale=opti_sigma)
    starting_tabs = "\t\t\t" if len(file) < 8 else "\t\t"
    starting_tabs = "\t" if len(file) > 18 else starting_tabs
    print("%s%s%.3f\t(%.3f, %.3f) | %.3f\t(%.3f, %.3f)" % (file, starting_tabs, orig_mean, conf_int_orig[0], conf_int_orig[1], opti_mean, conf_int_opti[0], conf_int_opti[1]))

if __name__ == "__main__":
    calc_conf(sys.argv[1])
