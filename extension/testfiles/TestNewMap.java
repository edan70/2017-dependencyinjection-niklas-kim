import java.util.Map;

public class TestNewMap {

  public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
    THURSDAY, FRIDAY, SATURDAY
  }
  @SuppressWarnings("unchecked")
	public static void main(String[] args) {

    // should inject TreeMap
    @Sorted Map<String, Integer> map1 = new Map<String, Integer>();
    // should inject EnumMap
    Map<Day, Integer> map2 = new Map<Day, Integer>();
    // should inject LinkedHashMap
    @InsertionSorted Map<String, Integer> map3 = new Map<String, Integer>();
    // should for now inject HashMap as default
    Map<String, Integer> map4 = new Map<>();

/*
    String a = "Kim";
    String b = "Niklas";
    Integer c = 1;
    Integer d = 2;
		map.put(a, c);
    map.put(b, d);
    */
	}
}
