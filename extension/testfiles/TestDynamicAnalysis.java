import java.util.Set;

public class TestDynamicAnalysis {

  public static void main(String[] args) {
    Set<Integer> ints = new Set<Integer>();
    Set<Float> floats = new Set<Float>();

    for (int i = 0; i < 1000000; i++) {
      ints.add(i);
      ints.add(i*2);
      floats.add(1.005f + i);
    }

    int sum = 0;
    float sumfloat = 0;
    for(Integer i : ints) {
        sum += i;
    }

    for(Float f : floats) {
      sumfloat += f;
    }

    //System.out.println("Total sum = " + sum);
  }

}
