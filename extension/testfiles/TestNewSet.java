import java.util.Set;
import java.util.Iterator;
//import java.util.HashSet;

public class TestNewSet {

  public enum TestEnum {
		test1, test2, test3
	}

  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    //Set<String> a = @Sorted new Set<String>();

    // should for now inject HashSet as default
    Set<String> b = new Set<String>();
    // should inject TreeSet
    @Sorted Set<String> testAuto = new java.util.Set<>();
    // should inject LinkedHashSet
    @InsertionSorted Set<String> c = new java.util.Set<String>();
    Set<String> d = new Set<>();
    // should inject EnumSet
    Set<TestEnum> testString = new Set<TestEnum>();
		//Set<TestEnum> testEnumAgain = new Set<>();

		testString.add(TestEnum.test1);
		testString.add(TestEnum.test3);
		testString.add(TestEnum.test2);

		Iterator<String> b_itr = b.iterator();

    b.add("Hej");
    b.add("Yo");
    b.add("look at this");
    b.remove("Hej");

    Iterator<String> d_itr = d.iterator();
    d.add("HEJJJJJJ");
    d.add("YOOOOOOOOO");
    d.add("ASDHASHDHAS");

  }
}
