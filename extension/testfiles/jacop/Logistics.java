import org.jacop.constraints.*;
import org.jacop.core.*;
import org.jacop.search.*;

public class Logistics {
	private static int graph_size;
	private static int start;
	private static int n_dests;
	private static int[] dest;
	private static int n_edges;
	private static int[] from;
	private static int[] to;
	private static int[] cost;

	public static void main(String[] args) {
		logistics(1);
	}

	private static void logistics() {

		Store store = new Store();

		// which edges has been walked on 0 = not walked on, 1 = walked on
		IntVar[] walked = new IntVar[n_edges];
		for (int i = 0; i < n_edges; i++) {
			walked[i] = new IntVar(store, "edge_"+(i+1), 0, 1);
		}

		// cost variable to be minimized
		IntVar totalCost = new IntVar(store,"cost", 0, 1000);


		// a vector containing the node values for the graph
		IntVar[] nodes = new IntVar[graph_size];
		for (int i = 1; i <= graph_size; i++) {
			nodes[i-1] = new IntVar(store, "node_"+i, i, i);
		}


		// a matrix containing subcircuit paths with different destinations
		IntVar[][] subcircuits = new IntVar[n_dests][graph_size];

		// creating subcircuits
		createSubCircuits(store, subcircuits);

		// adding constraints for calculating walked variable
		calculatingPath(store, walked, nodes, subcircuits);

		// sum of walked vector with cost weights
		store.impose(new LinearInt(store, walked, cost, "=", totalCost));


		//Search
		System.out.println(
				"Number of variables: " + store.size() + "\nNumber of constraints: " + store.numberConstraints());

		Search<IntVar> label = new DepthFirstSearch<IntVar>();

		SelectChoicePoint<IntVar> select = new SimpleMatrixSelect<IntVar>(subcircuits, null, new IndomainMin<IntVar>());

		boolean Result = label.labeling(store, select, totalCost);

		if (Result) {
			System.out.println("\n*** Yes");
			System.out.println("Cost : " + totalCost.value());

		}

			else System.out.println("\n*** No");

	}

	private static void calculatingPath(Store store, IntVar[] walked, IntVar[] nodes, IntVar[][] subcircuits) {

		for (int i = 0; i < n_dests; i++) {
			for (int j = 0; j < graph_size; j++) {
				for (int k = 0; k < n_edges; k++) {

					PrimitiveConstraint c1 = new XeqC(nodes[j], from[k]);
					PrimitiveConstraint c2 = new XeqC(subcircuits[i][j], to[k]);

					PrimitiveConstraint c3 = new XeqC(nodes[j], to[k]);
					PrimitiveConstraint c4 = new XeqC(subcircuits[i][j], from[k]);

					PrimitiveConstraint c5 = new XeqC(walked[k], 1);

					store.impose(new IfThen(new Or (new And(c1,c2), new And(c3,c4)), c5));

				}

			}
		}
	}

	private static void createSubCircuits(Store store, IntVar[][] subcircuits) {
		IntVar[] subCircuit;
		for (int i = 0; i < n_dests; i++) {
			subCircuit = new IntVar[graph_size];
			addNodesToSubCircuit(store, subCircuit);
			addPathsToSubCircuit(subCircuit);
			// dest points to start
			store.impose(new XeqC(subCircuit[dest[i]-1], start));

			// start can't point to itself
			store.impose(new XneqC(subCircuit[start-1], start));

			// create subcircuit
			store.impose(new Subcircuit(subCircuit));

			subcircuits[i] = subCircuit;
		}
	}

	private static void addPathsToSubCircuit(IntVar[] subcircuitPath) {
		for(int i = 1 ; i <= graph_size ; i ++) {
			for(int j = 0; j < n_edges; j++) {
				if(from[j] == i) {
					subcircuitPath[i-1].addDom(to[j], to[j]);
					subcircuitPath[to[j]-1].addDom(from[j],from[j]);
				}
			}

			// if the node is a destination it should have a path to start (0 cost)
			for(int k = 0 ; k < n_dests ; k++) {
				if(i == dest[k])
					subcircuitPath[i-1].addDom(start, start);
			}

		}
	}

	private static void addNodesToSubCircuit(Store store, IntVar[] subcircuitPath) {
		for(int i = 0 ; i < graph_size ; i ++) {
			subcircuitPath[i] = new IntVar(store, "node_"+(i+1),(i+1),(i+1));
		}
	}


	private static void logistics(int i) {
		switch(i) {
		case 1:
			setInputs(6, 1, 1, new int[]{6}, 7,
					new int[]{1,1,2,2,3,4,4}, new int[]{2,3,3,4,5,5,6}, new int[]{4,2,5,10,3,4,11});
			break;

		case 2:
			setInputs(6, 1, 2, new int[]{5,6}, 7,
					new int[]{1,1,2, 2,3,4, 4}, new int[]{2,3,3, 4,5,5, 6}, new int[]{4,2,5,10,3,4,11});
			break;

		case 3:
			setInputs(6, 1, 2, new int[]{5,6}, 9,
					new int[]{1,1,1,2,2,3,3,3,4}, new int[]{2,3,4,3,5,4,5,6,6}, new int[]{6,1,5,5,3,5,6,4,2});
			break;
		}

		logistics();

	}

	private static void setInputs(int size, int _start, int dests, int[] _dest, int edges, int[] _from, int[] _to, int[] _cost) {
		graph_size = size; start = _start; n_dests = dests; dest = _dest; n_edges = edges; from = _from;
		to = _to; cost = _cost;
	}
}
