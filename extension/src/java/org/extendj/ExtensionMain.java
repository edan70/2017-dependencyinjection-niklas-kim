package org.extendj;

import org.extendj.ast.CompilationUnit;
import org.extendj.ast.Program;

public class ExtensionMain extends JavaCompiler {

  public static void main(String args[]) {
    int exitCode = new ExtensionMain().run(args);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
  }

  @Override
  protected int processCompilationUnit(CompilationUnit unit) throws Error {
    // Replace the following super call to skip semantic error checking in unit.
    return super.processCompilationUnit(unit);
  }

  /** Called by processCompilationUnit when there are no errors in the argument unit.  */
  @Override
  protected void processNoErrors(CompilationUnit unit) {
    unit.process();
    System.out.println("\nGenerating class file...");
    super.processNoErrors(unit);
  }
}
